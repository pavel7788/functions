const {listOfPosts} = require("./posts");
const getSum = (str1, str2) => {
  if (typeof(str1)!=='string'||typeof(str2)!=='string')
    return false;

  let num1, num2;

  if (str1==='')
    num1 = 0;

  if (str2==='')
    num2 = 0;

  if ((num1!==0&&!Number.parseInt(str1))||(num2!==0&&!Number.parseInt(str2)))
      return false;

  if (num1 !==0)
    num1 = Number.parseInt(str1);
  if (num2 !==0)
    num2 = Number.parseInt(str2);

  return String (num1+num2);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let p_count = 0;
  let c_count = 0;
  for (let post of listOfPosts) {
    if (post.author === authorName)
      p_count++;
  }
  for (let post of listOfPosts) {
    if (post.comments) {
      post.comments.forEach( (c)=> {
        if (c.author === authorName)
          c_count++;
      } );
    }
  }
  return `Post:${p_count},comments:${c_count}`;
};

const tickets=(people)=> {

  if (people.length===0) {
    return "NO";
  }

  if (people[0]>25) {
    return "NO";
  }

  let cash = [];
  for (let i=0; i<people.length; ++i) {
    cash.push(25*i);
  }

  for (let i=0; i<people.length; i++) {
    if (people[i]-cash[i]>25) {
      return "NO";
    }
  }

  return "YES";
};



module.exports = {getSum, getQuantityPostsByAuthor, tickets};
